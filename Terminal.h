//
//  Terminal.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/6/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Camera;

@interface Terminal : NSManagedObject

@property (nonatomic, retain) NSNumber * terminalID;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * zipcode;
@property (nonatomic, retain) NSString * terminalName;
@property (nonatomic, retain) NSString * terminalAbbrev;
@property (nonatomic, retain) NSSet *cameras;
@end

@interface Terminal (CoreDataGeneratedAccessors)

- (void)addCamerasObject:(Camera *)value;
- (void)removeCamerasObject:(Camera *)value;
- (void)addCameras:(NSSet *)values;
- (void)removeCameras:(NSSet *)values;

@end
