//
//  Camera.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/6/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "Camera.h"
#import "Terminal.h"


@implementation _Camera

@dynamic id;
@dynamic latitude;
@dynamic longitude;
@dynamic roadName;
@dynamic title;
@dynamic url;
@dynamic terminal;

@end
