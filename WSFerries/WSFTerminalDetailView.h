//
//  WSFTerminalDetailView.h
//  WSFerries
//
//  Created by Jeff McLeman on 2/27/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WSFTerminalDetailViewController.h"

@interface WSFTerminalDetailView : NSView
@property (assign) IBOutlet id <WSFTerminalDetailControllerViewDelegate> delegate;
@end
