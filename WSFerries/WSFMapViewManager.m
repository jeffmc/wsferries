//
//  WSFMapViewManager.m
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import "WSFMapViewManager.h"
#import "WSFMapViewDelegate.h"
#import "WSFServiceManager.h"
#import "WSFMapViewController.h"
#import "WSFFerryAnnotation.h"
#import "WSFTerminalAnnotation.h"
#import "Log.h"
#import "WSFStrings.h"
#import "NSString+JSON_TO_STRING.h"

@interface WSFMapViewManager ()

@property (nonatomic,strong) WSFMapViewDelegate *mapViewDelegate;
@property (nonatomic, strong) dispatch_source_t refreshTimer;
@property (readwrite) BOOL refreshTimerRunning;
@end


@implementation WSFMapViewManager

-(instancetype)init {
    self = [super init];
    if (self) {
    
    }
    return self;
}

-(void)dealloc {
   // [self stopUpdateTimer];
    self.refreshTimer = nil;
    self.mapController = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setupMapView {
    MKMapView *mapView = self.mapController.mapView;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(47.546667, -122.468889);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = MKCoordinateRegionMake(location, span);
    [mapView setCenterCoordinate:region.center animated:YES];
    //mapView.showsUserLocation = YES;
    mapView.zoomEnabled = YES;
    [mapView setRegion:region animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMapNotification:) name:kWSFFerryRefreshVesselNotification object:nil];
    
    [self setupTerminalsOnMap];
    [self retrieveFerrySceduleData];
    [self retrieveFerryAlerts];
    [self startUpdateTimer];
    
    
    
}

-(void)setupTerminalsOnMap {
    __block NSArray *terminals = nil;
    __block NSArray *cameras = nil;
    [[WSFServiceManager sharedInstance] fetchTerminalData:^(id returnObject, NSError *error){
    
        __block NSError *localError = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            terminals = [NSJSONSerialization JSONObjectWithData:returnObject options:0 error:&localError];
            [[WSFServiceManager sharedInstance] setTerminalInfoArray:[terminals copy]];
            WSFTerminalAnnotation *annotation = nil;
            for (NSDictionary *dict in terminals) {
                annotation = [self.mapController annotationForTerminalID:[[dict objectForKey:kWSFTerminalID] integerValue]];
                if (!annotation) {
                    annotation = [[WSFTerminalAnnotation alloc] init];
                    annotation.localTitle = dict[kWSFTerminalName];
                    [annotation setLabelContents:annotation.localTitle];
                    annotation.terminalID = [dict[kWSFTerminalID] integerValue];
                }
                
                CGFloat lat = [dict[kWSFTerminalLatitude] doubleValue];
                CGFloat lon = [dict[kWSFTerminalLongitude] doubleValue];
                CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(lat, lon);
                annotation.coordinate = coordinates;
                [self.mapController setAnnotation:annotation];
            }
            [self.mapController.mapView setNeedsDisplay:YES];
        });
        __block NSMutableArray *wsfCameras = [NSMutableArray array];
        [[WSFServiceManager sharedInstance] fetchCameraData:^(id returnObject, NSError *error){
          __block NSError *localError = nil;
            cameras = [NSJSONSerialization JSONObjectWithData:returnObject options:0 error:&localError];
            
            [cameras enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSDictionary *camera = (NSDictionary *)obj;
                NSDictionary *cameraLocation = camera[@"CameraLocation"];
                NSString *roadName = cameraLocation[@"RoadName"];
                if ([roadName isEqualToString:@"Ferries"]) {
                    [wsfCameras addObject:[camera copy]];
                }
            }];
            [[WSFServiceManager sharedInstance] setCameraList:[wsfCameras copy]];
        }];
     }];
    
}


- (void)retrieveFerrySceduleData {
    __block NSArray *schedules = nil;
   
    [[WSFServiceManager sharedInstance] fetchScheduleData:^(id returnObject, NSError *error){
        __block NSError *localError = nil;
        schedules = [NSJSONSerialization JSONObjectWithData:returnObject options:0 error:&localError];
        if (!error && returnObject != nil) {
            [[WSFServiceManager sharedInstance] setScheduleList:[schedules copy]];
            DLog("Got Ferry Schedule Data");
        }
    }];

}

- (void)retrieveFerryAlerts {
    __block NSArray *alerts = nil;
    [[WSFServiceManager sharedInstance] fetchAlertData:^(id returnObject, NSError *error) {
        __block NSError *localError = nil;
        alerts = [NSJSONSerialization JSONObjectWithData:returnObject options:0 error:&localError];
        if (!error && !localError && returnObject !=nil) {
            [[WSFServiceManager sharedInstance] setFerryAlertList:[alerts copy]];
        }
    }];
}
     
- (void)refreshMapNotification:(NSNotification *)note {
    [self updateFerriesOnMap];
}
- (void)updateFerriesOnMap {
    __block NSArray *vesselArray = nil;
    [[WSFServiceManager sharedInstance] fetchFerryData:^(id returnObject, NSError *error){
        __block NSError *localError = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            vesselArray = [NSJSONSerialization JSONObjectWithData:returnObject options:0 error:&localError];
            [[WSFServiceManager sharedInstance] setVesselInfoArray:[vesselArray copy]];
            
            NSArray *ferryData = vesselArray;
            WSFFerryAnnotation *annotation = nil;
            for (NSDictionary *dict in ferryData) {
                annotation = [self.mapController annotationForVesselID:[[dict objectForKey:kWSFFerryVesselID] integerValue]];
                if (!annotation) {
                    annotation = [[WSFFerryAnnotation alloc] init];
                    [annotation setLocalTitle:[dict objectForKey:@"VesselName"]];
                    [annotation setLabelContents:annotation.localTitle];
                    annotation.vesselID = [[dict objectForKey:kWSFFerryVesselID] integerValue];
                    
                }
                CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([[dict objectForKey:@"Latitude"] doubleValue], [[dict objectForKey:@"Longitude"] doubleValue]);
                [annotation setCoordinate:coordinates];
                annotation.departed = [NSString getDateStringFromJSON:[dict objectForKey:@"LeftDock"]];
                annotation.eta = [NSString getDateStringFromJSON:[dict objectForKey:@"Eta"]];
                [self.mapController setAnnotation:annotation];
                
            }
            [self.mapController.mapView setNeedsDisplay:YES];
        });
    }];
}

- (void)startUpdateTimer {
    if (self.refreshTimer != NULL || self.refreshTimerRunning) return;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    self.refreshTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_time_t startTime = dispatch_time(DISPATCH_TIME_NOW, 0);
    uint64_t interval = 20 *NSEC_PER_SEC;
    dispatch_source_set_timer(self.refreshTimer, startTime, interval, 5000ull);
    
    dispatch_source_set_event_handler(self.refreshTimer, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            DLog("update Timer fired");
            [self updateFerriesOnMap];
        });
        
    });
    
    dispatch_resume(self.refreshTimer);
    self.refreshTimerRunning = YES;
    
}

-(void)stopUpdateTimer {
    
    dispatch_suspend(self.refreshTimer);
}

@end
