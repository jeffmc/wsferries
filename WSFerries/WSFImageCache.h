//
//  WSFImageCache.h
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^WSFImageCacheCompletion) (id image, NSError *error);

@interface WSFImageCache : NSObject

+(WSFImageCache *)sharedInstance;
-(void)fetchImageForFerry:(NSInteger)vesselID withCompletion:(WSFImageCacheCompletion)completion;
-(void)fetchImageForTerminal:(NSInteger)vesselID withCompletion:(WSFImageCacheCompletion)completion;
@end
