//
//  WSFMainWindowController.m
//  WSFerries
//
//  Created by Jeff McLeman on 2/28/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFMainWindowController.h"
#import "WSFStrings.h"
#import "Log.h"
#import "WSFScheduleWindowController.h"


static NSString * const kNSTitlebarAccessoryViewControllerName = @"NSTitlebarAccessoryViewController";

@interface WSFMainWindowController ()
@property (nonatomic,strong) IBOutlet NSTitlebarAccessoryViewController *accessoryController;

@end

@implementation WSFMainWindowController



- (void)awakeFromNib {
   /*  NSButton *toggleAddPopoverButton = ({
        NSButton *button = [[NSButton alloc] initWithFrame:NSMakeRect(0, 0, 23, 21)];
        [button.cell setControlSize: NSMiniControlSize];
        [button setButtonType: NSMomentaryChangeButton];
        [button setBezelStyle: NSRoundRectBezelStyle];
        [button setTitle: @""];
        [button setBordered: NO];
        [button setImage: [NSImage imageNamed: NSImageNameShareTemplate]];
        
        [button setTarget: self];
        [button setAction: @selector(shareButtonHandler:)];
        button;
    });

#if __MAC_OS_X_VERSION_MAX_ALLOWED < 101000
#error Use 10.10 SDK to compile this code
#endif
    NSTitlebarAccessoryViewController *accessoryViewController = [NSTitlebarAccessoryViewController new];
    accessoryViewController.view = toggleAddPopoverButton;
    accessoryViewController.layoutAttribute = NSLayoutAttributeRight;
    [self.window addTitlebarAccessoryViewController: accessoryViewController]; */
}


- (void)windowDidLoad {
    [super windowDidLoad];
    [self.window setTitle:kWSFFerriesApplicationName];
    
    
}

- (IBAction)shareButtonHandler:(id)sender {
    
    _scheduleWindowController = [[WSFScheduleWindowController alloc] initWithWindowNibName:@"WSFScheduleWindowController"];
    [self.scheduleWindowController showWindow:self];
}

@end
    
