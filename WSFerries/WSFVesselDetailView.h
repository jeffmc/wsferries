//
//  WSFVesselDetailView.h
//  WSFerries
//
//  Created by Jeff McLeman on 2/24/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WSFVesselDetailController.h"

@interface WSFVesselDetailView : NSView

@property (assign) IBOutlet id <WSFVesselDetailControllerDelegate> delegate;
@end
