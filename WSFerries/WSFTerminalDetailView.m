//
//  WSFTerminalDetailView.m
//  WSFerries
//
//  Created by Jeff McLeman on 2/27/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFTerminalDetailView.h"

@implementation WSFTerminalDetailView

-(void)mouseDown:(NSEvent *)theEvent {
    
    NSPoint event_location = [theEvent locationInWindow];
    NSPoint local_point = [self convertPoint:event_location fromView:nil];
    CGRect frame = [self frame];
    
    if ( (local_point.x < frame.size.width) &&
        (local_point.y < frame.size.height) ) {
        id <WSFTerminalDetailControllerViewDelegate> delegate = self.delegate;
        if ( [delegate respondsToSelector:@selector(closePopoverView)]) {
            [delegate closePopoverView];
        }
    }
    
}

@end
