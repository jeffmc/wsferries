//
//  WSFImageCache.m
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WSFImageCache.h"
#import "WSFServiceManager.h"
#import "Log.h"

static NSString * const FerryImageSlug = @"images/VesselWatch/boats/main/trimmed/lightgreen1_";

@interface WSFImageCache ()
@property (nonatomic,strong) NSMutableDictionary *imageCache;
@end

@implementation WSFImageCache

static WSFImageCache *s_imgCache = nil;

+(WSFImageCache *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_imgCache = [[WSFImageCache alloc] init];
    });
    
    return s_imgCache;
}

-(instancetype) init {
    self = [super init];
    if (self) {
        _imageCache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)dealloc {
    self.imageCache = nil;
}

+ (NSURLSession *)ourSession
{
    static NSURLSession *ourSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        [config setHTTPMaximumConnectionsPerHost:1];
        [config setTimeoutIntervalForRequest:25.0f];
        ourSession = [NSURLSession sessionWithConfiguration:config];
    });
    return ourSession;
}

-(void)fetchImageForFerry:(NSInteger)vesselID withCompletion:(WSFImageCacheCompletion) completion {
    
    NSDictionary *ferryInfo = [[WSFServiceManager sharedInstance] getVesselInfoWithID:vesselID];
    
    NSInteger heading = [[ferryInfo objectForKey:@"Heading"] longLongValue];
    NSInteger roundedHeading = heading;
    if (heading != 0 || heading != 90 || heading != 180 || heading != 270) {
        roundedHeading = roundup(heading, 5);
    }
    NSString *stringHeading = [NSString stringWithFormat:@"%ld",roundedHeading];

    NSString *slug = [NSString stringWithFormat:@"%@%@.png",FerryImageSlug,stringHeading];
    __block NSString *path = [NSString stringWithFormat:@"%@/%@",ASSET_HOST,slug];
    NSURL *urlForImage = [NSURL URLWithString:path];
    NSImage *image = self.imageCache[path];
    if (image != nil) {
       DLog("Found Ferry Position image in cache %@",path);
        completion(image,nil);
        return;
    }
    NSURLSession *session = [WSFImageCache ourSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlForImage];
    //NSURLSessionConfiguration *config = [session configuration];
    //[config setTimeoutIntervalForRequest:25.0];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;

        if (!error && [httpResponse statusCode] == 200) {
            NSImage *image = [[NSImage alloc] initWithData:data];
            DLog("adding Ferry Position image to cache %@",path);
            [self.imageCache setObject:image forKey:path];
            completion(image, error);
        } else {
            completion(nil,error);
        }
    
    
    }];
    [task resume];
}

-(void)fetchImageForTerminal:(NSInteger)terminalID withCompletion:(WSFImageCacheCompletion) completion {
    
    
    __block NSString *path = [NSString stringWithFormat:@"%@/%@",ASSET_HOST,@"images/VesselWatch/terminal.png"];
    NSURL *urlForImage = [NSURL URLWithString:path];
    NSImage *image = self.imageCache[path];
    if (image != nil) {
        DLog("Found Terminal image in cache %@",path);
        completion(image,nil);
        return;
    }
    NSURLSession *session = [WSFImageCache ourSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlForImage];
    //NSURLSessionConfiguration *config = [session configuration];
    //[config setTimeoutIntervalForRequest:25.0];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (!error && [httpResponse statusCode] == 200) {
            NSImage *image = [[NSImage alloc] initWithData:data];
            DLog("adding Terminal image to cache %@",path);
            [self.imageCache setObject:image forKey:path];
            completion(image, error);
        } else {
            completion(nil,error);
        }
        
        
    }];
    [task resume];
}

@end
