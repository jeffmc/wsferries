//
//  WSFImageView.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/11/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WSFTerminalDetailViewController.h"

@interface WSFImageView : NSImageView
@property (assign) id <WSFTerminalDetailControllerViewDelegate> delegate;
@end
