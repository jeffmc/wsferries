//
//  WSFFerryAnnotation.m
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import "WSFFerryAnnotation.h"

@implementation WSFFerryAnnotation


-(CLLocationCoordinate2D)coordinate {
    
    CLLocationCoordinate2D theCoordinate;
    
    theCoordinate.latitude = [[self latitude] doubleValue];
    theCoordinate.longitude = [[self longitude] doubleValue];
    return theCoordinate;
}

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    
    self.longitude = [NSNumber numberWithDouble:newCoordinate.longitude];
    self.latitude = [NSNumber numberWithDouble:newCoordinate.latitude];
}

-(NSString *)title {
    return _localTitle;
}

-(NSString *)subtitle {
    NSString *subtitleString = [NSString stringWithFormat:@"%@ Departed:%@, ETA:%@",self.label,
                                self.departed ,
                                self.eta ];
    return subtitleString;
}

-(void)setLabelContents:(NSString *)newLabel {
    self.label = newLabel;
}

@end
