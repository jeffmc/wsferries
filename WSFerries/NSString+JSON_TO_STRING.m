//
//  NSString+JSON_TO_STRING.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/21/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "NSString+JSON_TO_STRING.h"

@implementation NSString (JSON_TO_STRING)
+ (NSString *)getDateStringFromJSON:(NSString *)dateString
{
    if ([dateString isKindOfClass:[NSNull class]]) {
        return nil;
    }
    if ( ! [dateString hasPrefix:@"/Date("] )
    {
        return nil;
    }
    
    // .NET delivers dates in JSON like: "/Date(1292851800000+0100)/" where
    // 1292851800000 is milliseconds since 1970 and +0100 is the timezone
    // Split the string into "Date" and the rest of the string, separated by the '(' character
    NSArray *stringParts = [dateString componentsSeparatedByString:@"("];// Date ticks in stringParts[1]
    NSString *timePart = [stringParts objectAtIndex:1];
    
    // Find the end of the millisecond part of the date, which will be either the entire string or
    // the part before the time zone.
    NSUInteger timePos = 0;
    NSUInteger maxIdx = [timePart length];
    while ( (timePos < maxIdx) && [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[timePart characterAtIndex:timePos]])
    {
        ++timePos;
    }
    // Split the time string into milliseconds and zone (plus extra characters)
    NSString *timeString = [timePart substringToIndex:timePos];
    NSString *zoneString = [timePart substringFromIndex:timePos];
    NSTimeInterval secondsSince1970 = [timeString doubleValue] / 1000.0;
    NSDate *retVal = [NSDate dateWithTimeIntervalSince1970:secondsSince1970];
    // Now, if we have a zone string, make sure that it is likely the proper format.
    if (zoneString && ([zoneString length] >= 5))
    {
        // Calculate the time zone offset for this date relative to GMT.
        NSInteger offsetFromGMT = 0;
        if ([zoneString characterAtIndex:0] == '+')
        {
            offsetFromGMT = ([[zoneString substringWithRange:NSMakeRange(1, 2)] intValue] * 3600
                             + [[zoneString substringWithRange:NSMakeRange(3, 2)] intValue] * 60);
        }
        else if ([zoneString characterAtIndex:0] == '-')
        {
            offsetFromGMT = -([[zoneString substringWithRange:NSMakeRange(1, 2)] intValue] * 3600
                              + [[zoneString substringWithRange:NSMakeRange(3, 2)] intValue] * 60);
        }
        
        // Now, calculate *our* offset from GMT, and subtract that from the overall offset from GMT.
        // We then will subtract that value from the time to give us the local time.
        NSTimeZone *localZone = [NSTimeZone localTimeZone];
        offsetFromGMT -= [localZone secondsFromGMTForDate:retVal];
        if ( offsetFromGMT != 0 )
        {
            retVal = [retVal dateByAddingTimeInterval:-offsetFromGMT];
        }
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    [dateFormatter setDateFormat:@"MMM d, yyyy h:mm a"];
    
    NSString *newDateString = [dateFormatter stringFromDate:retVal];

    return newDateString;
}

@end
