//
//  WSFScheduleCellView.m
//  WSFerries
//
//  Created by Jeff McLeman on 7/11/15.
//  Copyright © 2015 Jeff McLeman. All rights reserved.
//

#import "WSFScheduleCellView.h"

@implementation WSFScheduleCellView

- (void)awakeFromNib
{
    [self.separatorView setWantsLayer:YES];
    CALayer *layer = self.separatorView.layer;
    layer.backgroundColor = [[NSColor grayColor] CGColor];
}
@end
