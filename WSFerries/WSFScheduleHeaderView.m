//
//  WSFScheduleHeaderView.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/15/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFScheduleHeaderView.h"
#import "Log.h"

@implementation WSFScheduleHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        _textField = [[NSTextField alloc] initWithFrame:self.frame];
        [_textField setBackgroundColor:[NSColor greenColor]];
        [_textField setTextColor:[NSColor blackColor]];
        [self addSubview:_textField];
    }
    return self;
}
-(void)mouseDown:(NSEvent *)theEvent {
    DLog("clicked in Header view of schedule table");
}
@end
