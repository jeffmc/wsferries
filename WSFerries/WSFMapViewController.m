//
//  WSFMapViewController.m
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import "WSFMapViewController.h"
#import "WSFMapViewManager.h"
#import "WSFFerryAnnotation.h"
#import "WSFTerminalAnnotation.h"

@interface WSFMapViewController ()
@property (nonatomic, strong) WSFMapViewManager *mapViewManager;
@end

@implementation WSFMapViewController

- (instancetype) initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _mapViewManager = [[WSFMapViewManager alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mapViewManager setMapController:self];
    [self.mapViewManager setupMapView];

}


-(void)dealloc {
    
    
}

-(void)setAnnotation:(id)annotation {
    
    @synchronized(self) {
        NSArray *annotations = [self.mapView annotations];
        if ([annotations containsObject:annotation]) {
            [self.mapView removeAnnotation:annotation];
        }
        
        [[self mapView] addAnnotation:annotation];
    }
}

-(void)clearAnnotations {
    
    @synchronized(self) {
        [self.mapView removeAnnotations:[self.mapView annotations]];
    }
}
-(WSFFerryAnnotation *)annotationForVesselID:(NSInteger)vesselID {
    
    @synchronized(self) {
        NSArray *tmpArray = [self.mapView.annotations copy];
        
        for (WSFFerryAnnotation *annotation in tmpArray) {
            
            if ([annotation isKindOfClass:[WSFFerryAnnotation class]] && annotation.vesselID ==vesselID) {
                return annotation;
            }
        }
    }
    return nil;
}

- (WSFTerminalAnnotation *)annotationForTerminalID:(NSInteger)terminalID {
    @synchronized(self) {
        NSArray *tmpArray = [self.mapView.annotations copy];
        
        for (WSFTerminalAnnotation *annotation in tmpArray) {
            
            if ([annotation isKindOfClass:[WSFTerminalAnnotation class]] && annotation.terminalID ==terminalID) {
                return annotation;
            }
        }
    }
    return nil;

}
@end
