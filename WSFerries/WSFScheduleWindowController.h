//
//  WSFScheduleWindowController.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/15/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WSFScheduleWindowController : NSWindowController
@property (readwrite) NSNumber *terminalID;
@end
