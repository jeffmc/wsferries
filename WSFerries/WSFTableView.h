//
//  WSFTableView.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/15/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol WSFTableViewDelegate <NSObject>

@optional
//Selection
-(BOOL)tableView:(NSTableView *)tableView shouldSelectRowAtIndexPath:(NSIndexPath *)indexPath;
-(BOOL)tableView:(NSTableView *)tableView shouldSelectSection:(NSInteger)section;
-(void)tableView:(NSTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@protocol WSFTableViewDataSource <NSObject>

//Number of rows in section
-(NSInteger)tableView:(NSTableView *)tableView numberOfRowsInSection:(NSInteger)section;

@optional

//Number of sections
-(NSInteger)numberOfSectionsInTableView:(NSTableView *)tableView;

//Has a header view for a section
-(BOOL)tableView:(NSTableView *)tableView hasHeaderViewForSection:(NSInteger)section;

//Height related
-(CGFloat)tableView:(NSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(CGFloat)tableView:(NSTableView *)tableView heightForHeaderViewForSection:(NSInteger)section;

//View related
-(NSView *)tableView:(NSTableView *)tableView viewForHeaderInSection:(NSInteger)section;
-(NSView *)tableView:(NSTableView *)tableView viewForIndexPath:(NSIndexPath *)indexPath;

@end

@interface WSFTableView : NSTableView <NSTableViewDataSource, NSTableViewDelegate>

@property (nonatomic, assign) IBOutlet id <WSFTableViewDataSource> wsfTableViewDataSource;
@property (nonatomic, assign) IBOutlet id <WSFTableViewDelegate> wsfTableViewDelegate;

-(NSIndexPath *)indexPathForView:(NSView *)view;

@end

//This is a reimplementation of the NSIndexPath UITableView category.
@interface NSIndexPath (NSTableView)

+ (NSIndexPath *)indexPathForRow:(NSInteger)row inSection:(NSInteger)section;

@property(nonatomic,readonly) NSInteger section;
@property(nonatomic,readonly) NSInteger row;

@end