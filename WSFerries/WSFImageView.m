//
//  WSFImageView.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/11/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFImageView.h"
#import "Log.h"

@implementation WSFImageView

-(void)mouseDown:(NSEvent *)theEvent {
    
    NSPoint event_location = [theEvent locationInWindow];
    NSPoint local_point = [self convertPoint:event_location fromView:nil];
    CGRect frame = [self frame];
    
    if ( (local_point.x < frame.size.width) &&
        (local_point.y < frame.size.height) ) {
 
        DLog("Clcked on Camera View");
        id <WSFTerminalDetailControllerViewDelegate> delegate = self.delegate;
        if ( [delegate respondsToSelector:@selector(launchLargeVideoView)]) {
            [delegate launchLargeVideoView];
        }

    }
    
}

@end
