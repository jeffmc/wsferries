//
//  WSFScheduleWindowController.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/15/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFScheduleWindowController.h"
#import "WSFScheduleCellView.h"
#import "Log.h"
#import "WSFTableView.h"

#import "WSFServiceManager.h"
#import "NSString+JSON_TO_STRING.h"
#import "AppDelegate.h"
#import "WSFStrings.h"

static NSString * const WSFScheduleCellViewIdentifier = @"WSFScheduleCellViewIdentifier";

@interface WSFScheduleWindowController () <NSWindowDelegate, WSFTableViewDataSource, WSFTableViewDelegate>
@property (weak) IBOutlet NSDatePicker *datePicker;
@property (nonatomic, strong) NSDictionary *scheduleDict;
@property (nonatomic, strong) NSArray *sailings;
@property (nonatomic, strong) NSDictionary *terminalSchedule;
@property (weak) IBOutlet WSFTableView *tableView;
@property (weak) IBOutlet NSScrollView *scrollView;
@property (weak) IBOutlet NSArrayController *scheduleItemsController;
@end

@implementation WSFScheduleWindowController

- (instancetype) initWithWindowNibName:(nonnull NSString *)windowNibName {
    self = [super initWithWindowNibName:windowNibName];
    if (self) {
        
    }
    return self;
}

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    self.scheduleDict = [[[WSFServiceManager sharedInstance] scheduleList] objectAtIndex:0];
    DLog("Schedule Date: %@", [NSString getDateStringFromJSON:self.scheduleDict[@"CacheDate"]]);
    NSArray *dates = self.scheduleDict[@"Date"];
    __weak typeof(self) weakself = self;
    for (NSDictionary *dict in dates) {
        __strong typeof(weakself) strongSelf = weakself;
        __block NSArray *filteredArray = nil;
        if (strongSelf) {
            strongSelf.sailings = dict[@"Sailings"];
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@",@"DepartingTerminalID",self.terminalID];
            filteredArray = [strongSelf.sailings filteredArrayUsingPredicate:pred];
            DLog("filtering");
            self.scheduleItemsController.content = filteredArray;
           /* __weak typeof(self) weakSelf = strongSelf;
            for (NSDictionary *terminalSched in strongSelf.sailings) {
                __strong typeof(weakSelf) strongSelf = weakSelf;
                NSNumber *tID =terminalSched[@"DepartingTerminalID"];
                DLog(@"terminalID = %@",tID);
                DLog("RouteAlert = %@",self.scheduleDict[@"RouteAlert"]);
                NSInteger tLong = [tID longValue];
                if ( tLong == strongSelf.terminalID) {
                    strongSelf.terminalSchedule = terminalSched;
                    
                }
            } */
        }
    }
    self.tableView.wsfTableViewDelegate = self;
    self.tableView.wsfTableViewDataSource = self;

    NSNib *nib = [[NSNib alloc] initWithNibNamed:@"WSFScheduleCellView" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib forIdentifier:WSFScheduleCellViewIdentifier];
    self.window.delegate = self;
}

- (void)dealloc {
    self.scheduleDict = nil;
}

#pragma mark WSFTableViewDataSource

-(NSInteger)tableView:(NSTableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.scheduleItemsController.arrangedObjects count];
}

-(NSInteger)numberOfSectionsInTableView:(NSTableView *)tableView
{
    return 1;
}

-(BOOL)tableView:(NSTableView *)tableView hasHeaderViewForSection:(NSInteger)section {
    return NO;
}

-(CGFloat)tableView:(NSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*NSView *view = [self tableView:tableView
                  viewForIndexPath:indexPath];
    
    CGFloat height = view.frame.size.height; */
    
    CGFloat height = 120;
    return height;
}

-(NSView *)tableView:(NSTableView *)tableView viewForIndexPath:(NSIndexPath *)indexPath {
    WSFScheduleCellView *cell = (WSFScheduleCellView *)[tableView makeViewWithIdentifier:WSFScheduleCellViewIdentifier owner:nil];
    if (cell == nil) {
        cell = [[WSFScheduleCellView alloc] init];
    }
    //cell.frame = CGRectMake(0, 0, 700, 50);
    //cell.backgroundColor = [NSColor redColor];
    return cell;
}

#pragma mark WSFTableViewDelegate
-(BOOL)tableView:(NSTableView *)tableView shouldSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLog("Selected section %ld, row %ld",(long)indexPath.section,(long)indexPath.row);
    return YES;

}

#pragma mark NSWindowDelegate;

- (void)windowWillClose:(NSNotification *)notification {
    AppDelegate *appDelegate = [[NSApplication sharedApplication] delegate];
    appDelegate.currentScheduleController = nil;
}

@end
