//
//  WSFStrings.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/11/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFStrings.h"

// terminals
NSString * const kWSFTerminalID = @"TerminalID";
NSString * const kWSFTerminalName = @"TerminalName";
NSString * const kWSFTerminalLatitude = @"Latitude";
NSString * const kWSFTerminalLongitude = @"Longitude";
NSString * const kWSFTerminalGISZoom = @"DispGISZoomLoc";

//ferries
NSString * const kWSFFerryVesselID = @"VesselID";


//cameras
NSString * const kWSFCameraKey = @"cameras";
NSString * const kWSFCameraItemsKey = @"items";


//notifications
NSString * const kWSFFerryRefreshVesselNotification = @"refreshNotification";


// application
NSString * const kWSFFerriesApplicationName = @"WSFerries";

@implementation WSFStrings

@end
