//
//  WSFSettingsWindowController.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/15/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFSettingsWindowController.h"
#import "Log.h"
#import "AppDelegate.h"

@interface WSFSettingsWindowController ()
@property (weak) IBOutlet NSButton *updateCheckbox;

@end

@implementation WSFSettingsWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    [self.window setTitle:@"WSF Ferries Settings"];
    
}

- (IBAction)updateCheckboxWasChecked:(id)sender {
    NSButton *checkbox = (NSButton *)sender;
    NSInteger state = [checkbox state];
    // convert to boolean, because to be OCD
    BOOL checkForUpdates = state ? YES : NO;
    DLog("Checkbox was clicked %ld", (long)state);
    AppDelegate *appDelegate = [[NSApplication sharedApplication] delegate];
    [appDelegate.appUpdater setAutomaticallyChecksForUpdates:checkForUpdates];
}
@end
