//
//  WSFMainWindowController.h
//  WSFerries
//
//  Created by Jeff McLeman on 2/28/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

@class WSFScheduleWindowController;

#import <Cocoa/Cocoa.h>

@interface WSFMainWindowController : NSWindowController
@property (nullable,strong) WSFScheduleWindowController *scheduleWindowController;
@end
