//
//  WSFStrings.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/11/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>

//terminals
extern NSString * const kWSFTerminalID;
extern NSString * const kWSFTerminalName;
extern NSString * const kWSFTerminalLatitude;
extern NSString * const kWSFTerminalLongitude;
extern NSString * const kWSFTerminalGISZoom;

//ferries
extern NSString * const kWSFFerryVesselID;


// cameras
extern NSString * const kWSFCameraKey;
extern NSString * const kWSFCameraItemsKey;

// notifications
extern NSString * const kWSFFerryRefreshVesselNotification;


//application
extern NSString * const kWSFFerriesApplicationName;

@interface WSFStrings : NSObject


@end
