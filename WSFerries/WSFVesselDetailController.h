//
//  WSFVesselDetailController.h
//  WSFerries
//
//  Created by Jeff McLeman on 2/24/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol WSFVesselDetailControllerDelegate <NSObject>

- (void)closePopoverView;

@end

@interface WSFVesselDetailController : NSViewController <WSFVesselDetailControllerDelegate>
@property (nonatomic, strong) NSNumber *vesselID;
@property (nonatomic, weak) IBOutlet NSPopover *popover;
- (instancetype) initWithNibName:(NSString *)nibNameOrNil vesselId:(NSNumber *)vesselID;
@end
