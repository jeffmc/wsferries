//
//  WSFTerminalDetailViewController.m
//  WSFerries
//
//  Created by Jeff McLeman on 2/27/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFTerminalDetailViewController.h"
#import "WSFServiceManager.h"
#import "WSFImageView.h"
#import "WSFCameraViewWindowController.h"
#import "Log.h"
#import "WSFScheduleWindowController.h"
#import "AppDelegate.h"
#import "WSFStrings.h"

@interface WSFTerminalDetailViewController ()
@property (nonatomic, strong) NSDictionary *terminalInfo;
@property (weak) IBOutlet NSTextField *terminalNameLabel;
@property (weak) IBOutlet WSFImageView *videoViewImage;
@property (weak) IBOutlet NSComboBox *videoPickerControl;
@property (weak) IBOutlet NSTextField *addressLabel;
@property (weak) IBOutlet NSTextField *cityLabel;
@property (weak) IBOutlet NSTextField *zipcodeLabel;
@property (strong,nonatomic) NSArray *camerasForTerminal;
@property (weak) IBOutlet NSImageView *cameraImageView;
@property (atomic, strong) dispatch_source_t refreshTimer;
@property (readwrite) BOOL refreshTimerRunning;
@property (nonatomic, strong) WSFCameraViewWindowController *cameraWindowController;
@property (nonatomic, strong) NSString *currentlyViewedCameraHREF;
@property (nonatomic, weak) IBOutlet NSButton *scheduleButton;
@end

@implementation WSFTerminalDetailViewController

- (instancetype) initWithNibName:(NSString *)nibNameOrNil terminalId:(NSNumber *)terminalID {
    self = [super initWithNibName:nibNameOrNil bundle:[NSBundle mainBundle]];
    if (self) {
        
        _terminalInfo = [[WSFServiceManager sharedInstance] getTerminalInfoWithTerminalID:[terminalID longValue]];
    }
    return self;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    NSDictionary *terminalInfo = self.terminalInfo;
    NSString *terminalName = [NSString stringWithFormat:@"%@ %@",
                              terminalInfo[@"TerminalName" ],@"Ferry Terminal"];
    [self.terminalNameLabel setStringValue:terminalName];
    [self.addressLabel setStringValue:terminalInfo[@"AddressLineOne"]];
    [self.cityLabel setStringValue:terminalInfo[@"City"]];
    [self.zipcodeLabel setStringValue:terminalInfo[@"ZipCode"]];
    [self buildCameraList:terminalInfo[@"TerminalName" ] withCompletion:^(NSArray *cameras){
        self.camerasForTerminal = [cameras copy];
        [self.videoPickerControl scrollItemAtIndexToVisible:0];
        [self.videoPickerControl reloadData];
        
    }];
    self.videoViewImage.delegate = self;
    [self.scheduleButton setHidden:YES];
}

-(void)viewWillAppear {
    [super viewWillAppear];
    [self.videoPickerControl selectItemAtIndex:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:NSComboBoxSelectionDidChangeNotification
                                                        object:self.videoPickerControl];

}
-(void)buildCameraList:(NSString *)ferryTerminalName withCompletion:(WSFTerminalDetailControllerViewSortCompletion)completion {
    NSArray *cameraList = [[[WSFServiceManager sharedInstance] cameraList] copy];
    NSArray *comp = [ferryTerminalName componentsSeparatedByString:@" "];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K contains %@",@"Title", comp[0]];
    NSArray *filteredArray = [cameraList filteredArrayUsingPredicate:predicate];
    completion(filteredArray);
}
#pragma mark NSComboBoxDataSource
-(NSInteger)numberOfItemsInComboBox:(NSComboBox *)aComboBox {
    return self.camerasForTerminal.count;
}

- (id)comboBox:(NSComboBox *)aComboBox objectValueForItemAtIndex:(NSInteger)index {
    
    NSDictionary *camera = [self.camerasForTerminal objectAtIndex:index];
    return camera[@"Title"];
}

- (NSUInteger)comboBox:(NSComboBox *)aComboBox indexOfItemWithStringValue:(NSString *)string {
    
    NSUInteger ret = 0;
    for (int i=0; i < self.camerasForTerminal.count; i++) {
        NSDictionary *dict = self.camerasForTerminal[i];
        NSString *title = dict[@"Title"];
        if ([title isEqualToString:string]) {
            ret = i;
            break;
        }
        
    }
    return ret;
}
#pragma mark NSComboBoxDelegate

- (void)comboBoxSelectionDidChange:(NSNotification *)notification {
    [self stopUpdateTimer];
    NSComboBox *box = [notification object];
    NSInteger index = [box indexOfSelectedItem];
    NSDictionary *camera = [self.camerasForTerminal objectAtIndex:index];
    NSString *jpgFile = camera[@"ImageURL"];
    self.currentlyViewedCameraHREF = jpgFile;
    NSURL *imageURL = [NSURL URLWithString:jpgFile];
    NSImage *image = [[NSImage alloc] initWithContentsOfURL:imageURL];
    [self.cameraImageView setImage:image];
    [self startVideoUpdateTimer:imageURL];
}

-(void)startVideoUpdateTimer:(NSURL *)urlToImage {
    if (self.refreshTimer != NULL || self.refreshTimerRunning) return;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    self.refreshTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_time_t startTime = dispatch_time(DISPATCH_TIME_NOW, 0);
    uint64_t interval = 30 *NSEC_PER_SEC;
    dispatch_source_set_timer(self.refreshTimer, startTime, interval, 5000ull);
    
    dispatch_source_set_event_handler(self.refreshTimer, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSImage *image = [[NSImage alloc] initWithContentsOfURL:urlToImage];
            [self.cameraImageView setImage:image];

        });
        
    });
    
    dispatch_resume(self.refreshTimer);
    self.refreshTimerRunning = YES;
    
}

-(void)stopUpdateTimer {
    if (self.refreshTimer) {
        dispatch_suspend(self.refreshTimer);
    }
}

- (IBAction)scheduleButtonPressed:(id)sender {
    DLog("Schedule Button Pressed");
    WSFScheduleWindowController *schedule = [[WSFScheduleWindowController alloc] initWithWindowNibName:@"WSFScheduleWindowController"];
    [schedule setTerminalID:self.terminalInfo[kWSFTerminalID]];
    [schedule showWindow:self];
    AppDelegate *appDelegate = [[NSApplication sharedApplication] delegate];
    appDelegate.currentScheduleController = schedule;
    
}

#pragma mark WSFTerminalDetailControllerViewDelegate

-(void)closePopoverView {
    [self stopUpdateTimer];
    [self.popover performClose:self];
}

- (void)launchLargeVideoView {

    _cameraWindowController = [[WSFCameraViewWindowController alloc] initWithWindowNibName:@"WSFCameraViewWindowController"];
    
   // [self.cameraWindowController setImageToDisplay:self.cameraImageView.image];
    [self.cameraWindowController setCameraFeedHREF:self.currentlyViewedCameraHREF];
    [self.cameraWindowController showWindow:self];
}
@end
