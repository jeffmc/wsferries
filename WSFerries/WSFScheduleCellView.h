//
//  WSFScheduleCellView.h
//  WSFerries
//
//  Created by Jeff McLeman on 7/11/15.
//  Copyright © 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WSFScheduleCellView : NSTableCellView
@property (weak) IBOutlet NSTextField *TerminalTextField;
@property (weak) IBOutlet NSTextField *ferryTextField;
@property (weak) IBOutlet NSTextField *arrivalTextField;
@property (weak) IBOutlet NSView *separatorView;

@property (weak) IBOutlet NSTextField *departureTextField;
@end
