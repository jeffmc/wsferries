//
//  WSFMapViewDelegate.h
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class WSFMapViewController;
@interface WSFMapViewDelegate : NSObject <MKMapViewDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) MKAnnotationView *selectedAnnotationView;
@property (nonatomic, weak) IBOutlet WSFMapViewController *mapViewController;
-(instancetype)initWithMapView:(MKMapView *)mapView;

@end
