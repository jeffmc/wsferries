//
//  WSFMapViewController.h
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <MapKit/MapKit.h>

@class WSFFerryAnnotation, WSFTerminalAnnotation;

@interface WSFMapViewController : NSViewController
@property (nonatomic, strong) IBOutlet MKMapView *mapView;

-(void)setAnnotation:(id)annotation;
-(void)clearAnnotations;
-(WSFFerryAnnotation *)annotationForVesselID:(NSInteger)vesselID;
- (WSFTerminalAnnotation *)annotationForTerminalID:(NSInteger)terminalID;
@end
