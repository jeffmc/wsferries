//
//  WSFCameraViewWindowController.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/11/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFCameraViewWindowController.h"
#import "Log.h"

@interface WSFCameraViewWindowController () <NSWindowDelegate>
@property (strong) dispatch_source_t refreshTimer;
@property (readwrite) BOOL refreshTimerRunning;
@end

@implementation WSFCameraViewWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    [self.window setDelegate:self];
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    [self cameraRefreshTimerEnable];
}

- (void)dealloc {
    _cameraFeedHREF = nil;
}

- (void)setCameraImage {
    NSURL *imageURL = [NSURL URLWithString:self.cameraFeedHREF];
    NSImage *image = [[NSImage alloc] initWithContentsOfURL:imageURL];
    [self.imageView setImage:image];
}

- (void)cameraRefreshTimerEnable {
    if (self.refreshTimer != NULL || self.refreshTimerRunning) return;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    self.refreshTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_time_t startTime = dispatch_time(DISPATCH_TIME_NOW, 0);
    uint64_t interval = 20 *NSEC_PER_SEC;
    dispatch_source_set_timer(self.refreshTimer, startTime, interval, 5000ull);
    
    dispatch_source_set_event_handler(self.refreshTimer, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            DLog("video refresh Timer fired");
            [self setCameraImage];
        });
        
    });
    
    dispatch_resume(self.refreshTimer);
    self.refreshTimerRunning = YES;
    
 
}

- (void)cameraRefreshDisable {
    dispatch_suspend(self.refreshTimer);
}

- (void)windowWillClose:(NSNotification *)notification {
    [self cameraRefreshDisable];
}
@end
