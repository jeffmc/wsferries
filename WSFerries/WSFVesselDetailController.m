//
//  WSFVesselDetailController.m
//  WSFerries
//
//  Created by Jeff McLeman on 2/24/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFVesselDetailController.h"
#import "WSFServiceManager.h"
#import "NSString+JSON_TO_STRING.h"

@interface WSFVesselDetailController ()
@property (nonatomic, weak) IBOutlet NSTextField *vesselTextField;
@property (nonatomic, weak) IBOutlet NSTextField *departTerminalTextField;
@property (nonatomic, weak) IBOutlet NSTextField *departTimeTextField;
@property (nonatomic, weak) IBOutlet NSTextField *arriveTerminalTextField;
@property (nonatomic, weak) IBOutlet NSTextField *arriveTimeTextField;
@property (nonatomic, weak) IBOutlet NSTextField *latTextField;
@property (nonatomic, weak) IBOutlet NSTextField *longTextField;
@property (nonatomic, strong) NSDictionary *vesselInfo;
@property (weak) IBOutlet NSTextField *departedLabel;
@property (weak) IBOutlet NSTextField *ArrivalLabel;
@property (weak) IBOutlet NSTextField *departTimeLabel;
@property (weak) IBOutlet NSTextField *vesselSpeedTextField;
@property (unsafe_unretained) IBOutlet NSTextView *etaBasisTextView;
@end

@implementation WSFVesselDetailController

- (instancetype) initWithNibName:(NSString *)nibNameOrNil vesselId:(NSNumber *)vesselID
{
    self = [super initWithNibName:nibNameOrNil bundle:[NSBundle mainBundle]];
    if (self) {
        
        _vesselInfo = [[WSFServiceManager sharedInstance] getVesselInfoWithID:[vesselID longValue]];
    }
    return self;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary *vesselInfo = self.vesselInfo;
    NSString *vesselName = [NSString stringWithFormat:@"%@ %@",@"MV",vesselInfo[@"VesselName"]];
    [self.vesselTextField setStringValue:vesselName];
    [self.departTimeTextField setStringValue:[NSString getDateStringFromJSON:vesselInfo[@"LeftDock"]]];
    [self.arriveTimeTextField setStringValue:[NSString getDateStringFromJSON:vesselInfo[@"Eta"]]];
    [self.departTerminalTextField setStringValue:vesselInfo[@"DepartingTerminalName"]];
    [self.arriveTerminalTextField setStringValue:vesselInfo[@"ArrivingTerminalName"]];
    [self.latTextField setStringValue:vesselInfo[@"Latitude"]];
    [self.longTextField setStringValue:vesselInfo[@"Longitude"]];
    NSString *speedString = [NSString stringWithFormat:@"%@ Knots",vesselInfo[@"Speed"]];
    [self.vesselSpeedTextField setStringValue:speedString];
    [self.etaBasisTextView setString:( vesselInfo[@"EtaBasis"] ?vesselInfo[@"EtaBasis"] : @"") ];
    if ([vesselInfo[@"AtDock"] boolValue]) {
        [self.departedLabel setStringValue:@"At:"];
        [self.ArrivalLabel setStringValue:@"Next Port:"];
        [self.departTimeTextField setStringValue:@"Now"];
        [self.departTimeLabel setStringValue:@"Departs:"];
        //[self.arriveTimeTextField setStringValue:vesselInfo[@"nextdep"]];
    }
}

-(void)closePopoverView {
    [self.popover performClose:self];
}
@end
