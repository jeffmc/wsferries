//
//  WSFServiceManager.h
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define HOST_URL @"http://mobileapp-wsdot.rhcloud.com"
//#define FERRY_WATCH @"/traveler/api/vesselwatch"
#define FERRY_WATCH @"http://www.wsdot.wa.gov/ferries/api/vessels/rest/vessellocations"
#define FERRY_TERMINALS @"http://www.wsdot.wa.gov/Ferries/API/Terminals/rest/terminallocations"
#define FERRY_CAMERAS @"http://wsdot.com/Traffic/api/HighwayCameras/HighwayCamerasREST.svc/GetCamerasAsJson"
#define ACCESS_TOKEN @"c53f883b-d26c-45ef-8cec-ce8f0b6347ae"
#define FERRY_SCHEDULE @"http://www.wsdot.wa.gov/Ferries/API/Schedule/rest"
#define ASSET_HOST @"http://www.wsdot.wa.gov/ferries"
#define FERRY_ALERTS @"http://www.wsdot.wa.gov/Ferries/API/Schedule/rest/alerts"
#define ROUTE_DETAILS @"http://www.wsdot.wa.gov/Ferries/API/Schedule/rest/routedetails/"

typedef void(^WSFServiceManagerCompletion)(id returnObject, NSError *status);

@interface WSFServiceManager : NSObject <NSURLSessionDelegate>

@property (nonatomic, strong) NSString *vesselWatchURL;
@property (nonatomic, strong) NSString *terminalWatchURL;
@property (nonatomic, strong) NSString *cameraURL;
@property (nonatomic, strong) NSString *scheduleURL;
@property (nonatomic, strong) NSArray *vesselInfoArray;
@property (nonatomic, strong) NSArray *terminalInfoArray;
@property (nonatomic, strong) NSArray *cameraList;
@property (nonatomic, strong) NSArray *scheduleList;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *ferryAlerts;
@property (nonatomic, strong) NSArray *ferryAlertList;

+(WSFServiceManager *)sharedInstance;

-(void)fetchFerryData:(WSFServiceManagerCompletion)completionBlock;

-(void)fetchFerryImage:(NSString *)imagePath completion:(WSFServiceManagerCompletion)completionBlock;

-(void)fetchTerminalData:(WSFServiceManagerCompletion)completionBlock;

-(void)fetchCameraData:(WSFServiceManagerCompletion)completionBlock;

- (void)fetchScheduleData:(WSFServiceManagerCompletion)completionBlock;

- (void)fetchAlertData:(WSFServiceManagerCompletion)completionBlock;

- (void)fetchRouteDetailsForDepartingTerminal:(NSNumber *)departingTerminalID arrivingTerminal:(NSNumber *)arrivingTerminalID date:(NSString *)date withCompletion:(WSFServiceManagerCompletion)completionBlock;

-(NSArray *)getVesselList;

-(NSDictionary *)getVesselInfo:(NSString *)vesselName;

-(NSDictionary *)getVesselInfoWithID:(NSInteger)vesselID;

-(NSDictionary *)getTerminalInfoWithTerminalID:(NSInteger)terminalID;

@end
