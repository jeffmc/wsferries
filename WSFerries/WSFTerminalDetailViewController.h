//
//  WSFTerminalDetailViewController.h
//  WSFerries
//
//  Created by Jeff McLeman on 2/27/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

typedef void (^WSFTerminalDetailControllerViewSortCompletion) (NSArray *cameras);

@protocol WSFTerminalDetailControllerViewDelegate <NSObject>

- (void)closePopoverView;

- (void)launchLargeVideoView;

@end

@interface WSFTerminalDetailViewController : NSViewController <WSFTerminalDetailControllerViewDelegate, NSComboBoxDataSource>
@property (nonatomic, strong) NSNumber *terminalID;
@property (nonatomic, weak) IBOutlet NSPopover *popover;
- (instancetype) initWithNibName:(NSString *)nibNameOrNil terminalId:(NSNumber *)terminalID;

@end
