//
//  WSFFerryAnnotation.h
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface WSFFerryAnnotation : NSObject  <MKAnnotation>
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *localTitle;
@property (nonatomic, strong) NSString *departed;
@property (nonatomic, strong) NSString *eta;
@property (nonatomic, strong) NSString *label;
@property (readwrite) NSInteger vesselID;
-(void)setLabelContents:(NSString *)newLabel;



@end
