//
//  WSFMapViewDelegate.m
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import "WSFMapViewDelegate.h"
#import "WSFServiceManager.h"
#import "Log.h"
#import "WSFFerryAnnotation.h"
#import "WSFImageCache.h"
#import "WSFVesselDetailController.h"
#import "WSFTerminalDetailViewController.h"
#import "WSFMapViewController.h"
#import "WSFTerminalAnnotation.h"
#import "WSFStrings.h"

#import "MKMapView+ZoomLevel.h"

@interface WSFMapViewDelegate ()
@property (nonatomic, strong) WSFVesselDetailController *detailController;
@property (nonatomic, strong) WSFTerminalDetailViewController *terminalDetailController;
@property (readwrite) BOOL regionWillChangeAnimatedCalled;
@property (readwrite) BOOL regionChangedBecauseAnnotationSelected;
@end

@implementation WSFMapViewDelegate

-(instancetype)initWithMapView:(MKMapView *)mapView {
    self = [super init];
    if (self) {
        self.mapView = mapView;
    }
    return self;
}

-(void)dealloc {
    self.mapView = nil;
    
}

#pragma mark MapViewDelegates

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        //return nil;
        static NSString *MyAnnotationIdentifier = @"myAnnotationIdentifier";
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[[self mapView] dequeueReusableAnnotationViewWithIdentifier:MyAnnotationIdentifier];
        if (!pinView) {
            
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:MyAnnotationIdentifier];
            //pinView.pinColor = MKPinAnnotationColorPurple;
            pinView.animatesDrop = YES;
            pinView.canShowCallout = YES;
        
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
        
    }  else if ([annotation isKindOfClass:[WSFFerryAnnotation class]]) {
        static NSString *LocationAnnotationIdentifier = @"locationAnnotationIdentifier";
        
        MKAnnotationView *pinView = (MKAnnotationView *)[[self mapView] dequeueReusableAnnotationViewWithIdentifier:LocationAnnotationIdentifier];
        if (!pinView) {
            
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                   reuseIdentifier:LocationAnnotationIdentifier];
        }
        WSFFerryAnnotation *ferryAnnotation = (WSFFerryAnnotation *)annotation;
        NSInteger vesselID = ferryAnnotation.vesselID;
        __block MKAnnotationView *blockPinView = pinView;
        [[WSFImageCache sharedInstance] fetchImageForFerry:vesselID withCompletion:^(id image, NSError *error) {
            if (error && image == nil) {
                return;
            }
            __block NSImage *ferryImage = image;

            dispatch_async(dispatch_get_main_queue(), ^{
                [blockPinView setImage:ferryImage];
                [blockPinView setNeedsDisplay:YES];
                //[blockPinView setCanShowCallout:YES];
            });
        }];
    
        pinView.annotation = annotation;
        return pinView;
        
    } else if ([annotation isKindOfClass:[WSFTerminalAnnotation class]]) {
        static NSString *TerminalAnnotationIdentifier = @"terminalAnnotationIdentifier";
        MKAnnotationView *pinView = (MKAnnotationView *)[[self mapView] dequeueReusableAnnotationViewWithIdentifier:TerminalAnnotationIdentifier];
        if (!pinView) {
            
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                            reuseIdentifier:TerminalAnnotationIdentifier];
        }
        WSFTerminalAnnotation *ferryAnnotation = (WSFTerminalAnnotation *)annotation;
        NSInteger terminalID = ferryAnnotation.terminalID;
        __block MKAnnotationView *blockPinView = pinView;
        [[WSFImageCache sharedInstance] fetchImageForTerminal:terminalID withCompletion:^(id image, NSError *error) {
            if (error && image == nil) {
                return;
            }
#if TARGET_OS_IOS
            __block UIImage *terminalImage = image;
#else
            __block NSImage *terminalImage = image;
#endif
            dispatch_async(dispatch_get_main_queue(), ^{
                [blockPinView setImage:terminalImage];
                //[blockPinView setCanShowCallout:YES];
            });
        }];
    
        pinView.annotation = annotation;
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    if (self.detailController != nil) {
        [self.detailController.popover performClose:self];
        self.detailController.popover = nil;
        self.detailController = nil;
    }
    if (self.terminalDetailController !=nil) {
        [self.terminalDetailController.popover performClose:self];
        self.terminalDetailController.popover = nil;
        self.terminalDetailController = nil;
    }
    id annotation = view.annotation;
    self.regionChangedBecauseAnnotationSelected = self.regionWillChangeAnimatedCalled;
    if ([annotation isKindOfClass:[WSFFerryAnnotation class]]) {
        
        DLog("Name=%@, vesselID=%ld",((WSFFerryAnnotation *)annotation).localTitle,(long)((WSFFerryAnnotation *)annotation).vesselID);
        self.detailController = [[WSFVesselDetailController alloc] initWithNibName:@"WSFVesselDetailController" vesselId:@(((WSFFerryAnnotation *)annotation).vesselID)];
        NSPopover *popOver = [[NSPopover alloc] init];
        [popOver setBehavior:NSPopoverBehaviorTransient];
        [popOver setContentViewController:self.detailController];
        [popOver setAnimates:YES];
        self.detailController.popover = popOver;
        [self.detailController.popover showRelativeToRect:[view bounds] ofView:view preferredEdge:NSMaxXEdge];

    } else if ([annotation isKindOfClass:[WSFTerminalAnnotation class]]) {
        DLog("Name=%@, vesselID=%ld",((WSFTerminalAnnotation *)annotation).localTitle,(long)((WSFTerminalAnnotation *)annotation).terminalID);
        self.terminalDetailController = [[WSFTerminalDetailViewController alloc] initWithNibName:@"WSFTerminalDetailViewController" terminalId:@(((WSFTerminalAnnotation *)annotation).terminalID)];
        NSPopover *popOver = [[NSPopover alloc] init];
        [popOver setBehavior:NSPopoverBehaviorTransient];
        [popOver setContentViewController:self.terminalDetailController];
        [popOver setAnimates:YES];
        self.terminalDetailController.popover = popOver;
        [self.terminalDetailController.popover showRelativeToRect:[view bounds] ofView:view preferredEdge:NSMaxXEdge];

    }
}

-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    self.regionWillChangeAnimatedCalled = YES;
    self.regionChangedBecauseAnnotationSelected = NO;
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (!self.regionChangedBecauseAnnotationSelected) //note "!" in front
    {
        [self terminalZoomHandler:mapView];
    }
    
    //reset flags...
    self.regionWillChangeAnimatedCalled = NO;
    self.regionChangedBecauseAnnotationSelected = NO;
}

- (void)terminalZoomHandler:(MKMapView *)mapView {
    NSArray *terminals = [[[WSFServiceManager sharedInstance] terminalInfoArray] copy];
    WSFTerminalAnnotation *annotation = nil;
    for (NSDictionary *dict in terminals) {
        annotation = [self.mapViewController annotationForTerminalID:[[dict objectForKey:kWSFTerminalID] integerValue]];
        NSArray *zoomArray = dict[kWSFTerminalGISZoom];
        NSInteger zoomLevel = (NSInteger)[mapView zoomLevel];
        for (NSDictionary *dict2 in zoomArray) {
            NSInteger dictZoom = [dict2[@"ZoomLevel"] integerValue];
            if ( dictZoom == zoomLevel) {
                CGFloat lat = [dict2[@"Latitude"] doubleValue];
                CGFloat lon = [dict2[@"Longitude"] doubleValue];
                CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(lat, lon);
                annotation.coordinate = coordinates;
                [mapView removeAnnotation:annotation];
                [mapView addAnnotation:annotation];
            }
        }
    }
}

@end
