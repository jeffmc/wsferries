//
//  WSFTerminalAnnotation.h
//  WSFerries
//
//  Created by Jeff McLeman on 2/25/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface WSFTerminalAnnotation : NSObject <MKAnnotation>
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *localTitle;
@property (nonatomic, strong) NSString *label;
@property (readwrite) NSInteger terminalID;
-(void)setLabelContents:(NSString *)newLabel;


@end
