//
//  WSFCameraViewWindowController.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/11/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WSFImageView.h"

@interface WSFCameraViewWindowController : NSWindowController
@property (nonatomic, weak) IBOutlet WSFImageView *imageView;
@property (nonatomic, strong)NSString *cameraFeedHREF;
@end
