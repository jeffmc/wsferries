//
//  NSString+JSON_TO_STRING.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/21/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JSON_TO_STRING)
+ (NSString *)getDateStringFromJSON:(NSString *)dateString;
@end
