//
//  WSFServiceManager.m
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import "WSFServiceManager.h"
#import "Log.h"
#import "WSFStrings.h"

@interface WSFServiceManager ()

@property (assign) WSFServiceManagerCompletion completionBlockFromCaller;

@end

@implementation WSFServiceManager

+ (WSFServiceManager *)sharedInstance {
    static WSFServiceManager *s_fsmgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_fsmgr = [[WSFServiceManager alloc] init];
    });
    return s_fsmgr;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.accessToken = ACCESS_TOKEN;
        self.vesselWatchURL = [NSString stringWithFormat:@"%@?apiaccesscode=%@", FERRY_WATCH, self.accessToken];
        self.terminalWatchURL = [NSString stringWithFormat:@"%@?apiaccesscode=%@",FERRY_TERMINALS, self.accessToken];
        self.cameraURL = [NSString stringWithFormat:@"%@?accesscode=%@", FERRY_CAMERAS,self.accessToken];
        self.scheduleURL = [NSString stringWithFormat:@"%@",FERRY_SCHEDULE];
        self.ferryAlerts = [NSString stringWithFormat:@"%@?apiaccesscode=%@", FERRY_ALERTS, self.accessToken];
    }
    return self;
}

- (void)dealloc {
    self.scheduleList = nil;
    self.terminalInfoArray = nil;
    self.vesselInfoArray = nil;
    self.accessToken = nil;
    self.vesselWatchURL = nil;
    self.terminalWatchURL = nil;
    self.cameraList = nil;
    self.cameraURL = nil;
    self.scheduleURL = nil;
    
}
- (void)fetchFerryData:(WSFServiceManagerCompletion)completionBlock {
    [self fetchServiceDataForURLString:self.vesselWatchURL withCompletion:completionBlock];
}

- (void)fetchFerryImage:(NSString *)imagePath completion:(WSFServiceManagerCompletion)completionBlock {
    
    //fetch the ferry image
    
    NSString *urlPathString = [NSString stringWithFormat:@"%@/%@",ASSET_HOST, imagePath];
    [self fetchServiceDataForURLString:urlPathString withCompletion:completionBlock];
}

- (void)fetchTerminalData:(WSFServiceManagerCompletion)completionBlock {
    [self fetchServiceDataForURLString:self.terminalWatchURL withCompletion:completionBlock];
}

- (void)fetchCameraData:(WSFServiceManagerCompletion)completionBlock {
    [self fetchServiceDataForURLString:self.cameraURL withCompletion:completionBlock];
}

- (void)fetchScheduleData:(WSFServiceManagerCompletion)completionBlock {
    [self fetchServiceDataForURLString:self.scheduleURL withCompletion:completionBlock];
}

- (void)fetchAlertData:(WSFServiceManagerCompletion)completionBlock {
    [self fetchServiceDataForURLString:self.ferryAlerts withCompletion:completionBlock];
}

- (void)fetchRouteDetailsForDepartingTerminal:(NSNumber *)departingTerminalID arrivingTerminal:(NSNumber *)arrivingTerminalID date:(NSString *)date withCompletion:(WSFServiceManagerCompletion)completionBlock
{
    
}
- (void)fetchServiceDataForURLString:(NSString *)urlString withCompletion:(WSFServiceManagerCompletion)completionBlock
{
    NSURLSession *session = [NSURLSession sharedSession ];
    NSURLSessionConfiguration *config = [session configuration];
    [config setTimeoutIntervalForResource:25.0];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                            
                                            completionBlock(data, error);
                                            
                                        }];
    
    [task resume];
    
   
}
#pragma mark NSURLSessionDelegate
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
    
    DLog("URL session did become invalid. Error = %@",error);
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    DLog("received auth challenge");
}
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session {
    
    DLog("URL session finished Events for BKGND");
}

#pragma mark vessel parsing

- (void)storeVesselInfo:(NSArray *)vesselArray {
    
    self.vesselInfoArray = [NSArray arrayWithArray:vesselArray];
}

- (NSArray *)getVesselList {
    
    return self.vesselInfoArray;
}

-(NSArray *)getTerminalList {
    return self.terminalInfoArray;
}

- (NSDictionary *)getVesselInfo:(NSString *)vesselName {
    
    NSArray *vesselList = [[self getVesselList] copy];
    
    for (NSDictionary *dict in vesselList) {
        
        NSString *name = [dict objectForKey:@"name"];
        
        if ([vesselName isEqualToString:name]) {
            
            return dict;
            break;
        }
    }
    return nil;
}

- (NSDictionary *)getVesselInfoWithID:(NSInteger)vesselID {
    NSArray *vesselList = [[self getVesselList] copy];
    
    for (NSDictionary *dict in vesselList) {
        
        NSInteger vID= [[dict objectForKey:kWSFFerryVesselID] integerValue];
        
        if (vesselID == vID) {
            
            return dict;
            break;
        }
    }
    return nil;
    
}

-(NSDictionary *)getTerminalInfoWithTerminalID:(NSInteger)terminalID {
    NSArray *terminalList = [[self getTerminalList] copy];
    
    for (NSDictionary *dict in terminalList) {
        NSInteger tID = [[dict objectForKey:@"TerminalID"] integerValue];
        if (terminalID == tID) {
            return dict;
            break;
        }
    }
    return nil;
}
@end
