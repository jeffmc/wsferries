//
//  WSFTerminalAnnotation.m
//  WSFerries
//
//  Created by Jeff McLeman on 2/25/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "WSFTerminalAnnotation.h"

@implementation WSFTerminalAnnotation
-(CLLocationCoordinate2D)coordinate {
    
    CLLocationCoordinate2D theCoordinate;
    
    theCoordinate.latitude = [[self latitude] doubleValue];
    theCoordinate.longitude = [[self longitude] doubleValue];
    return theCoordinate;
}

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    
    self.longitude = [NSNumber numberWithDouble:newCoordinate.longitude];
    self.latitude = [NSNumber numberWithDouble:newCoordinate.latitude];
}

-(NSString *)title {
    return _localTitle;
}

-(NSString *)subtitle {
    return nil;
}

-(void)setLabelContents:(NSString *)newLabel {
    self.label = newLabel;
}


@end
