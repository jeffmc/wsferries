//
//  Log.h
//  GitNote
//
//  Created by Jeff McLeman on 11/10/13.
//  Copyright (c) 2013 Jeff McLeman. All rights reserved.
//

#ifndef GitNote_Log_h
#define GitNote_Log_h

#ifdef DEBUG

#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#else

#define DLog(...)

#endif

#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);


#endif
