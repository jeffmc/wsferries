//
//  WSFMapViewManager.h
//  WSFerries
//
//  Created by Jeff McLeman on 9/2/14.
//  Copyright (c) 2014 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class WSFMapViewController;

@interface WSFMapViewManager : NSObject

@property (nonatomic, strong) WSFMapViewController *mapController;

-(void)setupMapView;
@end
