//
//  MKMapView+ZoomLevel.h
//  WSFerries
//
//  Created by Jeff McLeman on 4/23/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <MapKit/MapKit.h>

#define MERCATOR_RADIUS 85445659.44705395
#define MAX_GOOGLE_LEVELS 20

@interface MKMapView (ZoomLevel)

- (CGFloat)zoomLevel;
@end
