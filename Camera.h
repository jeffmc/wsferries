//
//  Camera.h
//  WSFerries
//
//  Created by Jeff McLeman on 3/6/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Terminal;

@interface _Camera : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * roadName;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) Terminal *terminal;

@end
