//
//  Terminal.m
//  WSFerries
//
//  Created by Jeff McLeman on 3/6/15.
//  Copyright (c) 2015 Jeff McLeman. All rights reserved.
//

#import "Terminal.h"
#import "Camera.h"


@implementation Terminal

@dynamic terminalID;
@dynamic address;
@dynamic city;
@dynamic country;
@dynamic latitude;
@dynamic longitude;
@dynamic state;
@dynamic zipcode;
@dynamic terminalName;
@dynamic terminalAbbrev;
@dynamic cameras;

@end
